import {Router} from "express";
// import {readAllUsers, readOneUser, deleteUser, updateUser, reloadUsersDb, addUser} from '../services/usersRouterServices.js'
import userService from "../services/usersRouterServices.js";


const usersRouter: Router = Router();

usersRouter.use(userService.reloadUsersDb());

usersRouter.post('/', userService.addUser())

usersRouter.put('/:id', userService.updateUser())

usersRouter.delete('/:id', userService.deleteUser());

usersRouter.get('/:id', userService.readOneUser());

usersRouter.get('/', userService.readAllUsers());

export default usersRouter;