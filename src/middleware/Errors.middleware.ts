import fs from 'fs/promises';
import { NextFunction, Request, Response } from 'express';
import {HttpException, UrlNotFoundException, ErrorResponse} from '../types/types.js';
import log from '@ajar/marker';

const { NODE_ENV } = process.env;

export const printErrorMiddleware =(err:HttpException,req:Request,res:Response,next:NextFunction) => { 
    log.magenta(err);
    next(err);
}

export const updateLogErrorMiddleware = (path:string) => (err:HttpException,req:Request,res:Response,next:NextFunction)=>{
    fs.writeFile(path,`${err.status} :: ${err.message} :: ${err.stack} \n`
    , {flag: "a",});
      next(err);
}

export const responseErrorMiddleware = (err:HttpException,req:Request,res:Response,next:NextFunction)=>{
    const errorResponse : ErrorResponse = {
        status: err.status || 500,
        message: err.message || 'Error'
    }
    if (NODE_ENV === 'development'){
        errorResponse.stack = err.stack;
    }
    res.status(errorResponse.status).json(errorResponse);
}

export const urlNotFoundMiddleware = (req: Request, res: Response, next: NextFunction) => {
    next(new UrlNotFoundException(req.url))
}

