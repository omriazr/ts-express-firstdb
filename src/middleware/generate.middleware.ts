import express, { Request, Response, NextFunction } from 'express';
import {generateID} from "../utils/utils.js";


export function generateReqID(){
    return (req:Request, res: Response, next: NextFunction):void => {
        req.id = generateID();
        next();
    }
}