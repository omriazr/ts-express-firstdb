import express, { RequestHandler, Express, Request, Response, NextFunction } from 'express';
import log from '@ajar/marker';
import morgan from 'morgan';
import usersRouter from './routers/usersRouter.js';
import fs from 'fs-extra';
import { generateReqID } from './middleware/generate.middleware.js';
import { updateLogFile } from './middleware/logFile.middleware.js';
import { fileURLToPath } from "url";
import path, { dirname } from "path";
import usersModel from './db/usersModel.js';
import {printErrorMiddleware, updateLogErrorMiddleware, responseErrorMiddleware, urlNotFoundMiddleware} from './middleware/Errors.middleware.js'


const {PORT, HOST} = process.env;

const __dirname:string = dirname(fileURLToPath(import.meta.url));
const LOG_FILE_PATH:string = path.resolve(__dirname, `./logs/users.logs.log`);
const ERRORS_LOG_FILE_PATH:string = path.resolve(__dirname, `./logs/users.errors.log`);
const DB_FILE_PATH:string = path.resolve(__dirname, './db.json');

const app:Express = express();

app.use(morgan('dev'));
app.use(express.json());

app.use(generateReqID());
fs.ensureFileSync(LOG_FILE_PATH);
app.use(updateLogFile(LOG_FILE_PATH));

app.use('/users', usersRouter);

app.use(urlNotFoundMiddleware);
app.use(printErrorMiddleware);
app.use(updateLogErrorMiddleware(ERRORS_LOG_FILE_PATH));
app.use(responseErrorMiddleware);

app.listen(Number(PORT), HOST as string,  async ()=> {
    await usersModel.initDB(DB_FILE_PATH);
    log.magenta(`🌎  listening on`,`http://${HOST}:${PORT}`);
});
