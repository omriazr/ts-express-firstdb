import express, { RequestHandler, Express, Request, Response, NextFunction } from 'express';
import usersModel from "../db/usersModel.js";
import { User, HttpException } from '../types/types.js';
import { generateID } from "../utils/utils.js";


class UserRouterService {
    reloadUsersDb() {
        return async (req: Request, res: Response, next: NextFunction) => {
            try {
                await usersModel.reload();
                req.users = usersModel.users;
                next();
            } catch (err) {
                next(err);
            }
        }
    }

    addUser() {
        return async (req: Request, res: Response, next: NextFunction) => {
            try {
                let user: User = {
                    id: generateID(),
                    ...req.body
                }
                req.users.push(user);
                await usersModel.save(req.users);
                res.status(200).send('create new user');
            } catch (err) {
                next(err);
            }
        }
    }

    updateUser() {
        return async (req: Request, res: Response, next: NextFunction) => {
            try {
                const userIndex = req.users.findIndex((user) => user.id === req.params.id);
                if (userIndex === -1) {
                    throw new HttpException(404, `user with id:${req.params.id} wasn't found, update user failed!`);
                }
                let updateUser: User = { ...req.users[userIndex], ...req.body };
                req.users[userIndex] = updateUser;
                await usersModel.save(req.users);
                res.status(200).send('user updated');
            } catch (err) {
                next(err);
            }
        }
    }

    deleteUser() {
        return async (req: Request, res: Response, next: NextFunction) => {
            try {
                const numberOfUsersBefore = req.users.length;
                req.users = req.users.filter((user) => user.id !== req.params.id);
                if (numberOfUsersBefore === req.users.length) {
                    throw new HttpException(404, `user with id:${req.params.id} wasn't found`);
                } else {
                    await usersModel.save(req.users);
                    res.status(200).send('user removed');
                }

            } catch (err) {
                next(err);
            }
        }
    }

    readOneUser() {
        return (req: Request, res: Response, next: NextFunction) => {
            try {
                const user = req.users.find((user) => user.id === req.params.id);
                if (user === undefined) {
                    throw new HttpException(404, `user with id:${req.params.id} wasn't found, read failed`);
                } else {
                    res.status(200).json(user);
                }

            } catch (err) {
                next(err);
            }
        }
    }

    readAllUsers() {
        return (req: Request, res: Response, next: NextFunction) => {
            try {
                res.json(req.users);
            } catch (err) {
                next(err);
            }
        }
    }

}

const instance = new UserRouterService();

export default instance;


// export const reloadUsersDb: RequestHandler = async (req: Request, res: Response, next: NextFunction) => {
//     try {
//         await usersModel.reload();
//         req.users = usersModel.users;
//         next();
//     } catch (err) {
//         next(err);
//     }
// }

// export const addUser = async (req: Request, res: Response, next: NextFunction) => {
//     try{
//         let user: User = {
//             id: generateID(),
//             ...req.body
//         }
//         req.users.push(user);
//         await usersModel.save(req.users);
//         res.status(200).send('create new user');
//     }catch(err){
//         next(err);
//     }
// }

// export const updateUser = async (req: Request, res: Response, next: NextFunction) => {
//     try {
//         const userIndex = req.users.findIndex((user) => user.id === req.params.id);
//         if (userIndex === -1) {
//             throw new HttpException(404, `user with id:${req.params.id} wasn't found, update user failed!`);
//         }
//         let updateUser: User = { ...req.users[userIndex], ...req.body };
//         req.users[userIndex] = updateUser;
//         await usersModel.save(req.users);
//         res.status(200).send('user updated');
//     } catch (err) {
//         next(err);
//     }
// }

// export const deleteUser = async (req: Request, res: Response, next: NextFunction) => {
//     try {
//         const numberOfUsersBefore = req.users.length;
//         req.users = req.users.filter((user) => user.id !== req.params.id);
//         if (numberOfUsersBefore === req.users.length) {
//             throw new HttpException(404, `user with id:${req.params.id} wasn't found`);
//         } else {
//             await usersModel.save(req.users);
//             res.status(200).send('user removed');
//         }

//     } catch (err) {
//         next(err);
//     }
// }

// export const readOneUser = (req: Request, res: Response, next: NextFunction) => {
//     try {
//         const user = req.users.find((user) => user.id === req.params.id);
//         if (user === undefined) {
//             throw new HttpException(404, `user with id:${req.params.id} wasn't found, read failed`);
//         } else {
//             res.status(200).json(user);
//         }

//     } catch (err) {
//         next(err);
//     }
// }

// export const readAllUsers = (req: Request, res: Response, next: NextFunction) => {
//     try {
//         res.json(req.users);
//     } catch (err) {
//         next(err);
//     }
// }
