export interface User {
    id: string,
    firstName: string,
    lastName: string,
    email: string,
    passowrd: number
}

export interface ErrorResponse {
    status: number;
    message: string;
    stack?: string;
  }
  

export class HttpException extends Error {

    status: number;
    message: string;

    constructor(status: number, message: string) {
        super(message);
        this.status = status;
        this.message = message;
    }
}

export class UrlNotFoundException extends HttpException {
    constructor(path: string) {
        super(404, `Url with path ${path} not found`);
    }
}

declare global {
    namespace Express {
        interface Request {
            users: User[];
            id: string;
        }
    }
}