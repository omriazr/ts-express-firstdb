import fs from 'fs/promises';
import { User } from "../types/types.js";

class UsersModel{

    dbFilePath:string = '';
    users: User[] = [];

    async initDB(dbFilePath:string){
        this.dbFilePath = dbFilePath;

        let file = await fs.open(this.dbFilePath, "a");
        const stat = await file.stat();
    
        if (stat.size > 0) {
            await this.reload()
        } else {
            await this.save();
        }
    }

    async reload(){
        this.users =  JSON.parse(await fs.readFile(this.dbFilePath, "utf-8"));
    }

    async save(users: User[] = []) {
        await fs.writeFile(this.dbFilePath, JSON.stringify(users,null,2));
    }
}

const instance = new UsersModel();
export default instance;